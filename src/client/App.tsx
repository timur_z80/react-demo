import * as React from 'react';
// import TicTacToe from './components/TicTacToe';
// import PeopleManager from './components/PeopleManager';
import Trello from './components/Trello/App';

const App = () => (
      <div className="App">
        {/* <PeopleManager /> */}
        <Trello />
      </div>
);

export default App;
