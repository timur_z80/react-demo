import * as React from 'react';

interface Props {
    onChangeNameOfX: React.ChangeEventHandler<HTMLInputElement>;
    onChangeNameOfO: React.ChangeEventHandler<HTMLInputElement>;
    nameOfX: string;
    nameOfO: string;
    onCreateGame: () => void;
}

const CreateGame = ({onChangeNameOfX, onChangeNameOfO, nameOfX, nameOfO, onCreateGame}: Props) => {
    return (
        <div>
            <div>
                <div>Name of Player X</div>
                <input type='text' onChange={onChangeNameOfX} value={nameOfX}/>
            </div>
            <div>
                <div>Name of Player O</div>
                <input type='text' onChange={onChangeNameOfO} value={nameOfO}/> 
            </div>
            <button onClick={onCreateGame}>Start</button>
        </div>
    );
}

export default CreateGame;