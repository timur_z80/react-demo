import * as React from 'react';
import Dialog from '../../Dialog';

interface Props {
    onClick: React.MouseEventHandler;
    onClickOutside?: React.MouseEventHandler
}

const DrawMessage = ({onClick, ...props}: Props) => {
    props.onClickOutside = onClick;
    return (
        <Dialog {...props}>
            It's a Draw, bro!
            <br />
            <br />
            <button onClick={onClick}>Close</button>
        </Dialog>
    )
}

export default DrawMessage;