import * as React from 'react';
import Dialog from '../../Dialog';

interface Props {
    onClick: React.MouseEventHandler;
    name: string;
    onClickOutside?: React.MouseEventHandler
}

const Congrats = ({onClick, name, ...props}: Props) => {
    props.onClickOutside = onClick;
    return <Dialog {...props}>
        <h3>Congratulations!</h3>
        <div><b>"{name}"</b> has won!</div>
        <br /><br />
        <button onClick={onClick}>Close</button>
    </Dialog>
}

export default Congrats;