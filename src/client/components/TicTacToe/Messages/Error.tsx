import * as React from 'react';
import Dialog from '../../Dialog';

interface Props {
    onClick: React.MouseEventHandler;
    onClickOutside?: React.MouseEventHandler;
    message: string;
}

const ErrorMessage = ({onClick, message, ...props}: Props) => {
    props.onClickOutside = onClick;
    return <Dialog {...props}>
        <h3>{message}</h3>
        <button onClick={onClick}>Close</button>
    </Dialog>
}

export default ErrorMessage;