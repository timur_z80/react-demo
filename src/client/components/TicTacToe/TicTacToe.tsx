import * as React from 'react';
import OccupiedCell from './Cell/OccupiedCell';
import Cell from './Cell/Cell';

interface Props {
    moves: {[key: string]: string},
    onClickOccupiedCell: () => void;
    onMove: (move: [number, number]) => () => void;
}

const TicTacToe = ({moves, onClickOccupiedCell, onMove}: Props) => {
        return (
            <div className="container">
                {
                    [[1,2,3], [1,2,3], [1,2,3]].map((set, index) => (
                        set.map(id => {
                            const moveIndex = `${index}${id}`;
                            const key = id*(index + 1);
                            return (
                                moves[moveIndex]
                                ? <OccupiedCell value={moves[moveIndex]} 
                                                key={key} 
                                                onClick={onMove([index, id])}
                                    />
                                : <Cell onClick={onMove([index, id])} key={key} />
                            )
                        }
                    )))            
                }
            </div>
        );
}

export default TicTacToe;