import * as React from 'react';

interface Props {
    value: string;
    onClick: React.MouseEventHandler;
}

const OccupiedCell = ({value, onClick}: Props) => {
    return <div className="item" onClick={onClick}><span>{value}</span></div>;
}

export default OccupiedCell;