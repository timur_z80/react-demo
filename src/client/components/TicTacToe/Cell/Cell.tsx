import * as React from 'react';

interface Props {
    onClick: React.MouseEventHandler;
}

const Cell = ({onClick}: Props) => {
    return <div className="item" onClick={onClick} />;
}

export default Cell;