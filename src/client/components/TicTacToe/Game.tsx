import * as React from 'react';
import TicTacToe from './TicTacToe';
import Congrats from './Messages/Congrats';
import DrawMessage from './Messages/DrawMessage';
import Error from './Messages/Error';

const server = 'http://tims-MacBook-Pro.local:8080/ttt';

const cellIsTakenMessage = 'This cell is already taken!';

interface State {
    hasWon: boolean;
    isDraw: boolean;
    moves: {[key:string]: string};
    winnerName: string;
    showError: boolean;
    message: string;
    currentPlayer: 'X'|'0';
}

interface Props {
    gameId: string;
    players: {X: string, '0': string};
    onGameEnd: () => void;
}

const initialState: State = {
    hasWon: false,
    moves: {},
    winnerName: '',
    isDraw: false,
    showError: false,
    message: '',
    currentPlayer: 'X'
};

class Game extends React.Component <Props, State> {
    state: State = initialState;

    onMove = (move: [number, number]) => () => { 
        const {currentPlayer} = this.state;
        const {gameId, players} = this.props;
        const moveData = {gameId, player: {name: players[currentPlayer], type: currentPlayer}, move: {X: move[0]+1, Y: move[1]}};
        fetch(`${server}/move/${JSON.stringify(moveData)}`)
            .then(response => response.json())
            .then(message => {
                if(message.status) {
                    this.setState(state => ({
                        ...state,
                        moves: {...state.moves, [`${move[0]}${move[1]}`]: currentPlayer},
                        message: message.status, 
                        hasWon: message.hasWon,
                        isDraw: message.isDraw,
                        winnerName: message.winnerName
                    }));
                }else {
                    this.setState(state => ({...state, message: message.message}))
                }
            })
            .catch(error => {
                this.setState({message: error.message});
                console.log('error: ', error);
            });
        
        this.setState((state:State) => ({...state, currentPlayer: state.currentPlayer === '0' ? 'X': '0'}))
    }

    onReset = () => {
        this.setState(initialState);
        this.props.onGameEnd();
    }

    onClickOccupiedCell = () => {
        this.setState({showError: true});
    }

    onHideError = () => {
        this.setState({showError: false});
    }

    render() {
        const {hasWon, winnerName, moves, showError, isDraw, message} = this.state;
        const {gameId} = this.props;
        return (
            <div className="tic-tac-toe">
                <h3>Tic Tac Toe</h3>
                <div className="message">{message}</div>
                {gameId && <TicTacToe 
                    moves={moves}
                    onClickOccupiedCell={this.onClickOccupiedCell}
                    onMove={this.onMove}
                />}
                {hasWon && <Congrats name={winnerName} onClick={this.onReset}/>}
                {isDraw && !hasWon && <DrawMessage onClick={this.onReset}/>}
                {showError && <Error onClick={this.onHideError} message={cellIsTakenMessage} />}
            </div>

        )
    }
}

export default Game;