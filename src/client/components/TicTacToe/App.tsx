import * as React from 'react';
import CreateGame from './CreateGame';
import Game from './Game';
import ErrorMessage from './Messages/Error';

const errorGameWasNotCreate = 'game was not created';
const server = 'http://tims-MacBook-Pro.local:8080/ttt';

interface AppState {
    players: {X: string, '0': string};
    gameId: string;
    message: string;
}

class App extends React.Component<{}, AppState>  {

    state: AppState = {
        players: {X: '', '0': ''},
        gameId: '',
        message: '',
    }

    onCreateGame = () =>  {
        const {players} = this.state;
        const data = [
            {name: players.X, type: 'X'},
            {name: players['0'], type: '0'}
        ];
        fetch(`${server}/create-game/${JSON.stringify(data)}`
            ).then(response => response.json())
            .then(result => this.setState({gameId: result.id}))
            .catch(error => this.setState({message: `${errorGameWasNotCreate}. ${error.message}`}));
    }

    onChangeName = (playerId: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.value;
        this.setState((state: AppState) => {     
            return {...state, players: {...state.players, [playerId]: name}};
        })
    }

    onMessageClose = () => {
        this.setState({message: ''});
    }

    onGameEnd = () => {
        this.setState({gameId: ''});
    }

    render() {
        const {gameId, players, message} = this.state;
        return (
            <div>
                {!gameId ? <CreateGame
                                onChangeNameOfX={this.onChangeName('X')}
                                onChangeNameOfO={this.onChangeName('0')}
                                onCreateGame={this.onCreateGame}
                                nameOfX={players.X}
                                nameOfO={players['0']}
                            />
                        :
                        <Game gameId={gameId} players={players} onGameEnd={this.onGameEnd}/>
                }
                {message && <ErrorMessage message={message} onClick={this.onMessageClose} />}
            </div>
        );
    }
}

export default App;