import * as React from 'react';

import './Person.css';


interface PersonProps {
    name: string;
}

const Person = ({name}: PersonProps) => {
    return <div className="Person">
        {name}
    </div>;
}

export default Person;