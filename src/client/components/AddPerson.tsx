import * as React from 'react';

interface Props {
    value: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onAdd: () => void;
}

const AddPerson = ({onAdd, onChange, value}: Props) => <div className="AddPerson">
        <input type="text" name='name' onChange={onChange} value={value}/>
        <button onClick={onAdd}>add</button>
    </div>;

export default AddPerson;