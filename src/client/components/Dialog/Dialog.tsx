import * as React from 'react';

import './style.css';

interface Props {
    onClickOutside?: React.MouseEventHandler
}

class Dialog extends React.Component <Props>{
    static defaultProps = {
        onClickOutside: () => {}
    }
    doNothing = (event: React.MouseEvent) => {
        event.stopPropagation();
    }

    render() {
        return (
            <div className='dialog-wrapper' onClick={this.props.onClickOutside}>
                <div className='dialog' onClick={this.doNothing}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Dialog;