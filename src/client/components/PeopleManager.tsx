import * as React from 'react';
import People from './People';
import AddPerson from './AddPerson';

interface State {
    people: string[],
    newPerson: string,
}


class PeopleManager extends React.Component <{}, State>{
    state: State = {
        people: [],
        newPerson: '',
    }

    componentDidMount() {
        this.getPeople();
    }

    onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({newPerson: event.target.value});
    }

    getPeople = () => {
        fetch('http://127.0.0.1:8080/people')
            .then(response => response.json())
            .then(result => this.setState(state => ({...state, people: result})));
    }

    onAdd = () => {
        fetch(`http://127.0.0.1:8080/add/${this.state.newPerson}`).then(this.getPeople);
    }

      render() {
        const {people, newPerson} = this.state;
        return <div className="PeopleManager">
                    <People people={people} />
                    <AddPerson value={newPerson} onChange={this.onChange} onAdd={this.onAdd}/>
                </div>
      }
}

export default PeopleManager;
