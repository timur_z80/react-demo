import * as React from 'react';
import Person from './Person';
// import uuid from 'uuid';


interface PeopleProps {
    people: string[];
}

const People = ({people}: PeopleProps) => {
    return <div>
        {people.map(person => <Person name={person} key={person}/>)}
    </div>;
}

export default People;