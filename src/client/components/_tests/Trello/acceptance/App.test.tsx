import * as React from 'react';

import { mount, ReactWrapper } from "enzyme";
import App from '../../../Trello/App';

import {v4 as uuid} from 'uuid';
import faker from 'faker';
import { mockFetch, fetchParams, CustomDragEvent } from '../../../Trello/Common/Common';

let newListId: string;
const listsIds: string[] = [];
let newCardId: string;
let cardNewValue: string;

type EnzymeElement = ReactWrapper;

describe('Trello component', () => {
    let tree: EnzymeElement;

    beforeEach(() => {
        global.fetch = mockFetch({lists: {}});
        tree = mount(<App />);
    });

    it("should have empty lists after load", () => {
        expect(tree.find('.list-wrapper')).toHaveLength(0);
    });

    it(" should add new list", async () => {
        const listName = faker.lorem.words(2);;
        await submitNewList(listName);

        expect(tree.find('.list-wrapper')).toHaveLength(1);
        expect(tree.find('.list-wrapper .list-header').contains(listName)).toBe(true);
    });

    it("should be able to add card in list and make sure it contains name of the card", async () => {
        const cardValue = faker.lorem.words(2);;
        await addListWithCard(cardValue);

        expect(tree.find('.list-cards .card .card-list-title').contains(cardValue)).toBe(true);
    });

    it("should be able to add cards to different lists", async () => {
        const cards = ['to do 1', 'to do 2'];
        await makeChainOfPromises(cards, addListWithCard);

        expect(getAllLists()).toHaveLength(2);

        const getFirstList = () => getAllLists().at(0);
        const getSecondList = () => getAllLists().at(1);

        expect(getFirstList().contains(cards[0])).toBe(true);
        expect(getFirstList().contains(cards[1])).toBe(false);

        expect(getSecondList().contains(cards[1])).toBe(true);
        expect(getSecondList().contains(cards[0])).toBe(false);
    });

    it("should be able to edit existing card text", async () => {
        await addListWithCard(faker.lorem.words(2));

        const editedValue = faker.lorem.words(3);
        expect(tree.find('.list-cards').contains(editedValue)).toBe(false);

        await simulateChangeCard(tree, editedValue);

        expect(tree.find('.list-cards').contains(editedValue)).toBe(true);
    });

    it("should drag and drop card to another list", async () => {
        const cards = ['to do 1', 'to do 2'];
        await makeChainOfPromises(cards, addListWithCard);

        const firstList = () => getAllLists().at(0);
        const secondList = () => getAllLists().at(1);

        const card1 = firstList().find('.card');

        expect(card1).toHaveLength(1);
        expect(secondList().find('.card')).toHaveLength(1);

        await simulateDragCartToList(card1, secondList());

        expect(secondList().find('.card')).toHaveLength(2);
        expect(firstList().find('.card')).toHaveLength(0);
        
    });

    it("should call get lists url after mount", () => {
        tree = mount(<App />);

        expect(global.fetch.calledUrl).toContain('lists/get');
    });

    it("should call add list url", () => {
        const listName = faker.lorem.words(2);
        submitNewList(listName);

        expect(global.fetch.calledParams).toEqual(fetchParams({method: 'POST', body: {name: listName}}));
        expect(global.fetch.calledUrl).toContain('list/add');
    });

    it("should call add card url", async () => {
        const name = faker.lorem.words(2);
        await addListWithCard(name);

        expect(global.fetch.calledParams).toEqual(fetchParams({method: 'POST', body: {listId: newListId, name}}));
        expect(global.fetch.calledUrl).toContain('card/add');
    });

    it("should call move card", async () => {
        await simulateDragAndDropCard();

        const fromListId = listsIds.pop();
        const toListId = listsIds.pop();

        expect(global.fetch.calledParams).toEqual(fetchParams({body: {fromListId, toListId, cardId: newCardId}, method: 'PUT'}));
        expect(global.fetch.calledUrl).toContain('card/move');
    });

    it("should call update card", async () => {
        await simulateCardUpdate();

        expect(global.fetch.calledParams).toEqual(fetchParams({body: {listId: newListId, cardId: newCardId, name: cardNewValue}, method: 'PUT'}));
        expect(global.fetch.calledUrl).toContain('card/update');
    });

    const getAllLists = () => {
        return tree.find('.list-wrapper');
    }

    async function submitNewList (listName: string) {
        newListId = uuid();
        listsIds.push(newListId);

        global.fetch = mockFetch({listId: newListId});
        const form = tree.find('.js-add-list form');
    
        simulateChange(form.find('input'), listName);
        await form.find('button').simulate('click');

        await Promise.all([Promise.resolve()]);
        tree.update();
    }

    async function addListWithCard (cardName: string) {
        const title = faker.lorem.words(1);
        await submitNewList('App test list ' + title);
        
        global.fetch = mockFetch({cardId: uuid()});
        await openFormForCardAdding(getAllLists().last());
        await addCardWithName(getAllLists().last(), cardName);

        await Promise.all([Promise.resolve()]);
        tree.update();
    }

    async function addCardWithName (tree: EnzymeElement, cardValue: string) {
        simulateChange(tree.find('.card-composer textarea'), cardValue);

        newCardId = uuid();
        global.fetch = mockFetch({cardId: newCardId});
        await tree.find('button').simulate('click');

        return Promise.resolve();
    }
    
    const openFormForCardAdding = async (tree: EnzymeElement) => {
        await Promise.resolve();
        tree.find('.open-card-composer').simulate('click');
    }

    const simulateDragCartToList = async (card: ReactWrapper, list: ReactWrapper) => {
        const dragEvent = new CustomDragEvent();

        card.simulate('dragStart', dragEvent);

        global.fetch = mockFetch({status: 'ok'});
        await list.simulate('drop', dragEvent);

        await Promise.all([Promise.resolve()]);        
        tree.update();
    }

    const simulateDragAndDropCard = async () => {
        const cards = ['to do 1', 'to do 2'];
        await makeChainOfPromises(cards, addListWithCard);

        await simulateDragCartToList(
            getAllLists().at(1).find('.card'),
            getAllLists().at(0)
        );
    }

    const simulateCardUpdate = async () => {
        cardNewValue = faker.lorem.words(3);
        await addListWithCard(faker.lorem.words(4));
        await simulateChangeCard(tree, cardNewValue);
    }

    const simulateChange = (element: ReactWrapper, targetValue: string) =>{
        element.simulate('change', {target: {value: targetValue}});
    }

    const simulateChangeCard = async (element: ReactWrapper, targetValue: string) => {
        element.find('.card').simulate('click');
        const editForm = element.find('.list-cards .card-composer');
        simulateChange(editForm.find('textarea'), targetValue);

        global.fetch = mockFetch({status: 'ok'});
        await editForm.find('button').simulate('click');

        await Promise.all([Promise.resolve()]);
        tree.update();
    }

    function makeChainOfPromises <T>(iterable: Array<T>, func: Function): Promise<T> {
        let promise = new Promise<T>((resolve) => resolve());

        iterable.map(i => promise = promise.then(() => func(i)));

        return promise;
    }
});