import * as React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import uuid = require('uuid');

import { Lists } from 'src/client/components/Trello/Board/Board';
import withApi from '../../../../Trello/HOC/withApi';
import { mockFetch, AddList, AddCard, UpdateCard, MoveCard, ApiProps, fetchParams } from '../../../../Trello/Common/Common';

const server = 'http://localhost:8080';

class TestComponent extends React.Component <ApiProps & {}, {}>{}

describe("withApi HOC", () => {
    let tree: ShallowWrapper;

    const lists = {};

    beforeEach(() => {
        global.fetch = mockFetch({lists});
        const WithApi = withApi<{}>(TestComponent);
        tree = shallow(<WithApi />);
    })

    it("should return component with 'lists' filled", async () => {
        await tree.instance().componentDidMount!();

        expect(tree.prop('lists')).toEqual(lists);
        expect(global.fetch.calledUrl).toBe(`${server}/lists/get`);
    });

    describe("should wrap component that requires", () => {
    
        it("'onAddList' prop", async () => {
            const onAddList = tree.prop('onAddList') as AddList;
            const response = {listId: uuid()};
            global.fetch = mockFetch(response);
    
            const listName = 'new list';
    
            const listId = await onAddList({name: listName});
    
            expect(global.fetch.calledUrl).toBe(`${server}/list/add/`);
            expect(global.fetch.calledParams).toEqual(fetchParams({body: {name: listName}, method: 'POST'}));
            expect(listId).toEqual(response.listId);
        });
    
        it("'onAddCard' prop", async () => {
            const onAddCard = tree.prop('onAddCard') as AddCard;
            const response = {cardId: uuid()};
            global.fetch = mockFetch(response);
    
            const cardName = 'new card';
    
            const addCardParams = {listId: uuid(), name: cardName};
            const cardId = await onAddCard(addCardParams);
    
            expect(global.fetch.calledUrl).toBe(`${server}/card/add/`);
            expect(global.fetch.calledParams).toEqual(fetchParams({method: 'POST', body: addCardParams}));
            expect(cardId).toEqual(response.cardId);
        });
    
        it("'onUpdateCard' prop", async () => {
            const onUpdateCard = tree.prop('onUpdateCard') as UpdateCard;

            const params = {listId: uuid(), cardId: uuid(), name: 'new card name'};
            await onUpdateCard(params);

            expect(global.fetch.calledParams).toEqual(fetchParams({body: params, method: 'PUT'}));
            expect(global.fetch.calledUrl).toContain(`${server}/card/update`);

        });

        it("'onMoveCard' prop", async () => {
            const onMoveCard = tree.prop('onMoveCard') as MoveCard;

            const params = {fromListId: uuid(), toListId: uuid(), cardId: uuid()};
            await onMoveCard(params);

            expect(global.fetch.calledUrl).toBe(`${server}/card/move/`, fetchParams({body: params, method: 'PUT'}));
        });
    })
});