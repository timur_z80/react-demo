import * as React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';

import OpenedCardForm from '../../../Trello/OpenedCardForm';

xdescribe("Test CardComposer", () => {
    const eventsCount = {change: 0, submit: 0};
    const props = {
            onCardChange: () => {eventsCount.change += 1},
            onCardSubmit: () => {eventsCount.submit += 1},
            title: 'Some title',
    };

    let form: ShallowWrapper; 

    beforeEach(()=>{
        form = shallow(<OpenedCardForm {...props}/>);
    })

    it("should have textarea", () => {
        expect(form.find('textarea')).toHaveLength(1);
    });

    it("should pass title prop to textarea", () => {
        expect(form.find('textarea').prop('value')).toBe(props.title);
    });

    it("should have button", () => {
        expect(form.find('button')).toHaveLength(1);
        expect(form.find('button').contains('Add Card')).toBe(true);
    });

    it("should call onCardSubmit when button was clicked", () => {
        form.find('button').simulate('click');

        expect(eventsCount.submit).toBe(1);
    });

    it("should call onCardChange when textarea was changed", () => {
        const characters = ['o', 'n', 'e'];
        characters.forEach(character => {
            form.find('textarea').simulate('change', {value: character});
        });

        expect(eventsCount.change).toBe(characters.length);
    });
});