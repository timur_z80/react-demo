import * as React from 'react';

import Board from '../../../Trello/Board/Board';
import AddList from '../../../Trello/AddList';
import ListItem from '../../../Trello/ListItem';

import { shallow } from "enzyme";
import {v4 as uuid} from 'uuid';


describe("Board", () => {

    const props = {
        lists: {},
        onAddList: jest.fn(() => getPromiseResolve(uuid())),
        onAddCard: () => { return getPromiseResolve(uuid())},
        onUpdateCard: () => {return getPromiseResolve({status: 'ok'})},
        onMoveCard: () => {return getPromiseResolve({status: 'ok'})},
    }
    const board = shallow(<Board {...props}/>);

    it("board should have 'add list' block", () => {
        expect(board.find(AddList)).toHaveLength(1);
    });

    it("should have empty lists", () => {
        expect(board.find(ListItem)).toHaveLength(0);
    });

    it("should show ListItem after it was added", async () => {
        const addList = board.find(AddList);
        const onListNameChange = addList.prop('onNameChange');
        const onSubmitNewList = addList.prop('onSubmit');

        const listName = 'new list';

        const event = {target: {value: listName}} as any;
        const mouseEvent = new MouseEvent('click') as any;

        onListNameChange(event);
        await onSubmitNewList(mouseEvent);

        expect(board.find(ListItem)).toHaveLength(1);
    });
})

const getPromiseResolve = (data = {}) => {
    return new Promise<{}>((resolve)=>{resolve(data)});
}