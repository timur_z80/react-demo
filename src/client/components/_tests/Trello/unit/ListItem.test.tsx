import * as React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';

import ListItem from '../../../Trello/ListItem';
import Card from '../../../Trello/Card';
import OpenedCardForm from '../../../Trello/OpenedCardForm';
import AddCard from '../../../Trello/AddCard';

xdescribe("Test List Item", () => {
    let item: ShallowWrapper;
    let mainContainer: ShallowWrapper;;
    const props = {
        name: 'some name',
        onCardSubmit: () => {},
        onClickForEdit: (cardId: string) => () => {},
        onNewCardNameChange: () => {},
        onSubmitEditedCardName: () => {},
        newCardTitle: '',
        editCardId: '',
        cards: {},
    };

    beforeEach(() => {
        item = shallow(<ListItem {...props} />);
        mainContainer = item.find('.list-wrapper');
    });

    it("should be a container with classes", () => {
        expect(mainContainer).toHaveLength(1);
    });

    it("should have header with list name", () => {
        const header = mainContainer.find('.list .list-header');
        expect(header).toHaveLength(1);
        expect(header.contains(props.name)).toBe(true);
    });

    it("should have empty cards list", () => {
        expect(mainContainer.find(Card)).toHaveLength(0);
    });

    it("should not show edit card form", () => {
        expect(mainContainer.find(OpenedCardForm)).toHaveLength(0);
    });

    it("should show add cart component", () => {
        expect(mainContainer.find(AddCard)).toHaveLength(1);
    });

    it("should have onDrop and onDragOver props to be functions", () => {
        expect(typeof item.prop('onDragOver')).toBe('function');
        expect(typeof item.prop('onDrop')).toBe('function');
    });

    it("should show card if it was passed", () => {
        item = shallow(<ListItem {...props} cards={{one: 'one'}} />);

        expect(item.find(Card)).toHaveLength(1);
    });
});