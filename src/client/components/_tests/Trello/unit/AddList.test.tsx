import * as React from 'react';

import { shallow } from 'enzyme';
import AddList from '../../../Trello/AddList';

const anonymFunc = () => {};

xdescribe("Test Add List Block", () => {
    const tree = shallow(<AddList onNameChange={anonymFunc} onSubmit={anonymFunc}/>);
    const addButton = tree.find('button');

    it("should have button and input", () => {
        expect(tree.find('form input')).toHaveLength(1);
        expect(addButton).toHaveLength(1);
    });

    it("add button should say 'Add List'", () => {
        expect(addButton.contains('Add List')).toBe(true);
    });
});