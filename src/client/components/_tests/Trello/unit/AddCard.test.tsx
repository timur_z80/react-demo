import * as React from 'react';

import { mount, shallow } from 'enzyme';
import AddCard from '../../../Trello/AddCard';
import OpenedCardForm from '../../../Trello/OpenedCardForm';
import ClosedCardComposer from '../../../Trello/ClosedCardComposer';

xdescribe("Test add card", () => {
    const title = 'new card';
    let showForm = false;
    const props = {onCardChange: () => {}, onCardSubmit: () => {}, title, showForm};
    
    it("should have form opener and hidden add card form", () => {
        const addCard = shallow(<AddCard {...props} />);

        expect(addCard.find(OpenedCardForm)).toHaveLength(0);
        expect(addCard.find(ClosedCardComposer)).toHaveLength(1);
    });

    it("should show form and hide form opener when prop showForm is true", () => {
        const addCard = shallow(<AddCard {...props} showForm={true} />);

        expect(addCard.find(OpenedCardForm)).toHaveLength(1);
        expect(addCard.find(OpenedCardForm).prop('title')).toBe(title);
        expect(addCard.find(ClosedCardComposer)).toHaveLength(0);
    });
});