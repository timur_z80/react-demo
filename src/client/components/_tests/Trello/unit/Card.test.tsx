import * as React from 'react';

import { shallow } from 'enzyme';
import Card from '../../../Trello/Card';

xdescribe("Test Card", () => {
    const title = 'My card';
    const card = shallow(<Card title={title} id={'1'}/>);

    it("should have container class", () => {
        expect(card.find('.card')).toHaveLength(1);
    });

    it("should have card's title", () => {
        expect(card.contains(title)).toBe(true);
    });

    it("should be draggable card", () => {
        expect(card.prop('draggable')).toBe(true);
    });

    it("should have onDragStart function", () => {
        expect(typeof card.prop('onDragStart')).toBe('function');
    });
});