import { Lists } from '../Board/Board';

export interface MockedFetch {
    calledUrl: string;
    calledParams: {[key:string]: any};
    (url: string): Promise<{}>;
}

export const mockFetch = (data = {}) => {
    const fetch: any = (url: string, params: {[key:string]: any}) => {
        fetch.calledUrl = url;
        fetch.calledParams = {...params};
        return Promise.resolve({json: () => data});
    }
    fetch.calledUrl = '';
    fetch.calledParams = {};

    return fetch as MockedFetch;
}

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

export const fetchParams = 
        ({method, body}: {method: string, body: {[key: string]: any}}) => 
            ({headers, method, body: JSON.stringify(body)});

const Common = () => {};

export default Common;

export interface AddList {
    ({name}: {name: string}): Promise<{}>; 
}

export interface AddCard {
    ({listId, name}: {listId: string, name: string}): Promise<{}>; 
}

export interface UpdateCard {
    ({listId, cardId, name}: {listId: string, cardId: string, name: string}): Promise<{}>;
}

export interface MoveCard {
    ({fromListId, toListId, cardId}: {fromListId: string, toListId: string, cardId: string}): Promise<{}>;
}

export interface ApiProps {
    lists: Lists;
    onAddList: AddList;
    onAddCard: AddCard;
    onUpdateCard: UpdateCard;
    onMoveCard: MoveCard;
}

export class CustomDragEvent {
    private value: string;

    dataTransfer = {
        setData: (key: string, value: string) =>  { this.value = value },
        getData: (key: string) => {return this.value;},
    }
}