import * as React from 'react';

interface CardComposerProps {
    onCardChange: React.ChangeEventHandler<HTMLTextAreaElement>;
    onCardSubmit: React.MouseEventHandler;
    onHideAddCard?: ()=>void;
    title?: string;
    buttonTitle?: string;
}

const doNothing = (event: React.MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
}

class OpenedCardForm extends React.Component <CardComposerProps> {
    private refInput = React.createRef<HTMLTextAreaElement>();

    static defaultProps = {
        buttonTitle: 'Add Card',
    }

    componentDidMount() {
        const textarea = this.refInput.current;
        if(textarea) textarea.focus();
    }

    onEnterPress: React.KeyboardEventHandler = (event) => {
        if(event.key === 'Enter') {
            event.preventDefault();
            
            const newEvent = new MouseEvent('click') as any;
            this.props.onCardSubmit(newEvent);
        }
    }

    render() {
        const {onCardChange, onCardSubmit, title = '', onHideAddCard=()=>{}, buttonTitle} = this.props;
        const onBlur = () => {
            setTimeout(onHideAddCard, 20);
        }
        return (
            <div className="card-composer" onBlur={onBlur} >
                <form>
                    <textarea onChange={onCardChange} 
                                onKeyPress={this.onEnterPress} 
                                placeholder="Enter a title for this card..." 
                                value={title} ref={this.refInput} 
                    />
                    <button className="primary" onClick={onCardSubmit}>{buttonTitle}</button>
                </form>
            </div>
        );
    }
}

export default OpenedCardForm;