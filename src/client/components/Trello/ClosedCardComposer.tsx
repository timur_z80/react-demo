import * as React from 'react';

interface OpenCardComposerProps {
    onShowAddCart?: React.MouseEventHandler;
    cardsCount?: number;
}

function ClosedCardComposer({onShowAddCart = ()=>{}, cardsCount=0}: OpenCardComposerProps) {
    return (
        <a className="open-card-composer js-open-card-composer" href="#" onClick={onShowAddCart}>
            <span className="icon-sm icon-add"></span>
            { cardsCount === 0
                ? <span className="js-add-a-card">Add a card</span>
                : <span className="js-add-a-card">Add another card</span>
            }
        </a>
    );
}

export default ClosedCardComposer;