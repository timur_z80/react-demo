import * as React from 'react';
import AddCard from './AddCard';
import Card from './Card';
import OpenedCardForm from './OpenedCardForm';

interface ListItemProps {
    name: string;
    cards: {[key: string]: string};
    editCardId?: string;
    newCardTitle: string;
    onNewCardNameChange: React.ChangeEventHandler<HTMLTextAreaElement>;
    onSubmitEditedCardName: React.MouseEventHandler;
    onClickForEdit: (cardId: string) => () => void;
    onCardSubmit: React.MouseEventHandler;
    onDrop?: React.DragEventHandler<HTMLDivElement>;
}

interface State {
    showForm: boolean;
}

class ListItem extends React.Component <ListItemProps, State> {

    state: State = {
        showForm: false,   
    }

    constructor(props:ListItemProps) {
        super(props);
    }

    onShowAddCard = (event: React.MouseEvent<HTMLAnchorElement>) => {
        event.stopPropagation();
        this.setState({showForm: true});
    }

    onHideAddCard = () => {this.setState({showForm: false})}

    onDragOver = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        console.log(event.target);
        const target = event.target as Node;
    }

    render() {
        const {showForm} = this.state;
        const {name, cards, editCardId, newCardTitle, onDrop=() => {}} = this.props;
        const {onCardSubmit, onClickForEdit, onNewCardNameChange, onSubmitEditedCardName} = this.props;
        return (
            <div className="list-wrapper" onDragOver={this.onDragOver} onDrop={onDrop}>
                <div className="list">
                    <div className="list-header">
                        {name}
                    </div>
                    <div className="list-cards">
                        {Object.keys(cards).map(key => {
                                { return editCardId !== key
                                    ? <Card title={cards[key]} onClickForEdit={onClickForEdit(key)} key={key} id={key}/>
                                    : <OpenedCardForm 
                                        onCardChange={onNewCardNameChange} 
                                        onCardSubmit={onSubmitEditedCardName} 
                                        onHideAddCard={this.onHideAddCard}
                                        title={newCardTitle}
                                        buttonTitle='Save'
                                        key={key}
                                    />
                                }
                            }
                        )}
                    </div>
                    <AddCard 
                            showForm={showForm}
                            onShowAddCart={this.onShowAddCard}
                            onHideAddCard={this.onHideAddCard}
                            onCardSubmit={onCardSubmit}
                            onCardChange={onNewCardNameChange}
                            title={newCardTitle}
                            cardsCount={Object.keys(cards).length}
                    />
                </div>
            </div>
        );
    }
}

export default ListItem;