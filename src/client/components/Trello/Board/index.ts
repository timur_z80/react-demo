import Board from './Board';

import './style.css';
import withApi from '../HOC/withApi';

export default withApi<{}>(Board);