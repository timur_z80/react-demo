import * as React from 'react';

import {v4 as uuid} from 'uuid';

import AddList from '../AddList';
import ListItem from '../ListItem';
import { ApiProps } from '../Common/Common';

export type Lists = {[key:string]: {name: string, cards: {[key: string]: string}}};

interface BoardState {
    lists: Lists,
    newListName: string,
    editCardId: string|undefined;
    newCardTitle: string;
}

class Board extends React.Component<ApiProps, BoardState>  {
    state: BoardState = {
        lists: {},
        newListName: '',
        editCardId: undefined,
        newCardTitle: ''
    };

    constructor(props: ApiProps) {
        super(props);

        this.state.lists = props.lists;
    }

    componentWillReceiveProps(newProps: ApiProps) {
        this.setState({lists: newProps.lists});
    }

    onListNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value: newListName} = event.target;

        this.setState(state => ({...state, newListName}));
    }

    onAddList = async (event: React.MouseEvent) => {
        event.preventDefault();
        event.stopPropagation();

        const {newListName} = this.state;

        if(!newListName) return;

        const listId = await this.props.onAddList({name: newListName}) as string;

        this.setState(state => {
            const lists = {...state.lists};
            lists[listId] = {name: newListName, cards: {}};
        
            return {...state, newListName: '', lists};
        });
    }

    onNewCardNameChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {        
        const {value} = event.target;

        this.setState({newCardTitle: value});
    }

    onSubmitEditedCardName = (listId: string) => async () => {
        const {editCardId, newCardTitle} = this.state;
        if(typeof editCardId === 'undefined' || !newCardTitle) return;
        
        const status = 
            await this.props.onUpdateCard({listId, cardId: editCardId, name: newCardTitle}) as string;
            
        if(status !== 'ok') return;

        this.setState(state => {
            const lists = {...state.lists};
            const cards = {...lists[listId].cards};
            cards[editCardId] = newCardTitle;
            lists[listId].cards = cards;

            return {...state, lists: {...lists}, newCardTitle: '', editCardId: undefined}
        });

    }

    onClickForEdit = (listId: string) => (cardId: string) => () => {
        this.setState({editCardId: cardId, newCardTitle: this.state.lists[listId].cards[cardId]});
    }

    onCardSubmit = (listId: string) => async (event: React.MouseEvent) => {
        event.preventDefault();
        const {newCardTitle} = this.state;

        if(!newCardTitle) return;

        const cardId = await this.props.onAddCard({listId, name: newCardTitle}) as string;

        this.setState(state => {
            const lists = {...state.lists};
            const cards = {...lists[listId].cards, [cardId]: newCardTitle};
            lists[listId].cards = cards;
            
            return {...state, lists: {...lists}, newCardTitle: ''}
        });
    }

    onDrop = (toListId: string) => async (
        event: React.DragEvent<HTMLDivElement>
    ) => {
        const cardId = event.dataTransfer.getData('text');
        const {lists} = this.state;
        const fromListId = Object.keys(lists).find(key => !!lists[key].cards[cardId]);

        if(!fromListId || fromListId === toListId) return;

        const status = 
            await this.props.onMoveCard({fromListId, toListId, cardId}) as string;
        
        if(status !== 'ok') return;

        this.setState(state => {
            const lists = {...state.lists};
            const card = lists[fromListId].cards[cardId];
            delete lists[fromListId].cards[cardId];

            lists[toListId].cards[cardId] = card;
            
            return {...state, lists};
        });
    }

    render() {
        const {lists, newListName, newCardTitle, editCardId} = this.state;
        
        return (
            <div id="board">
                {Object.keys(lists).map(key => (
                    <ListItem 
                        name={lists[key].name}
                        key={key}
                        cards={lists[key].cards}
                        onCardSubmit={this.onCardSubmit(key)}
                        onClickForEdit={this.onClickForEdit(key)}
                        onNewCardNameChange={this.onNewCardNameChange}
                        onSubmitEditedCardName={this.onSubmitEditedCardName(key)}
                        newCardTitle={newCardTitle}
                        editCardId={editCardId}
                        onDrop={this.onDrop(key)}
                />)
                )
                }
                <div className="js-add-list">
                    <AddList 
                        value={newListName}
                        onNameChange={this.onListNameChange} 
                        onSubmit={this.onAddList.bind(this)} 
                    />
                </div>
            </div>
        );
    }
}

export default Board;