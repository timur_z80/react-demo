import * as React from 'react';

interface CardProps {
    id: string;
    title: string;
    onClickForEdit?: () => void;
}

const onDragStart = (id: string) => (
    event: React.DragEvent<HTMLAnchorElement>
) => {
    event.dataTransfer.setData('text/plain', id);
}

function Card({id, title, onClickForEdit=()=>{}}: CardProps) {
    return (
        <a className="card" onClick={onClickForEdit} draggable={true} onDragStart={onDragStart(id)} >
            <span className="card-list-title">{title}</span>
        </a>
    );
}

export default Card;