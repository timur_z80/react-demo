import * as React from 'react';
import OpenedCardForm from './OpenedCardForm';
import ClosedCardComposer from './ClosedCardComposer';

interface AddCardProps {
    showForm?: boolean;
    onCardChange: React.ChangeEventHandler<HTMLTextAreaElement>;
    onCardSubmit: React.MouseEventHandler;
    title?: string;
    cardsCount?: number;
    onShowAddCart?: React.MouseEventHandler;
    onHideAddCard?: ()=>void;
}

function AddCard({showForm = false, ...props}: AddCardProps) {
    return (
        <React.Fragment>
            {showForm 
                ? <OpenedCardForm {...props} />
                : <ClosedCardComposer {...props} />
            }
        </React.Fragment>
    );
}

export default AddCard;