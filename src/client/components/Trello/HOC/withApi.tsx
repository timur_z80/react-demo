import * as React from 'react';

import {AddList, AddCard, UpdateCard, MoveCard, ApiProps, fetchParams} from '../Common/Common';
import { Lists } from '../Board/Board';

const server = 'http://localhost:8080';

const loadList = () => {
    return fetch(`${server}/lists/get`)
    .then(response => response.json())
    .then(response => {
        if(!response.lists){
            console.log('response does not have lists key');
            return;
        }
        return response.lists;
    })
    .catch(error => console.log('fetch exception:',error.message));
}

const onAddList: AddList = async ({name}) => {
    const response = await fetch(`${server}/list/add/`, fetchParams({ method: 'POST', body: { name } }));
    const response_1 = await response.json();
    return response_1.listId;
}

const onAddCard: AddCard = ({listId, name}) => {
    return fetch(`${server}/card/add/`, fetchParams({body: {listId, name}, method: 'POST'}))
    .then(response => response.json())
    .then(response => {
        return response.cardId;
    })
}

const onUpdateCard: UpdateCard = ({listId, cardId, name}) => {
    return fetch(`${server}/card/update/`, fetchParams({body: {listId, cardId, name}, method: 'PUT'}))
    .then(response => response.json())
    .then(response => {
        return response.status;
    })
}

const onMoveCard: MoveCard = ({fromListId, toListId, cardId}) => {
    return fetch(`${server}/card/move/`, fetchParams({body: {fromListId, toListId, cardId}, method: 'PUT'}))
    .then(response => response.json())
    .then(response => {
        return response.status;
    })
}

const withApi = <T extends {}>(
    Component: React.ComponentType<T & ApiProps>
    ) => {
    return class WithApi extends React.Component<T, Lists>  {
        state: Lists = {}

        async componentDidMount() {
            this.setState(await loadList());
        }

        render() {
            return (
                <Component 
                    {...this.props}
                    lists={this.state}
                    onAddList={onAddList}
                    onAddCard={onAddCard}
                    onUpdateCard={onUpdateCard}
                    onMoveCard={onMoveCard}
                />
            );
        }
    }
}

export default withApi;