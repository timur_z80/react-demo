import * as React from 'react';
import Board from './Board';

function App() {
    return (
        <div>
            <h2 className="main-header">
                <img src="https://a.trellocdn.com/prgb/dist/images/header-logo-2x.01ef898811a879595cea.png" />
            </h2>
            
            <Board />
        </div>
    );
}

export default App;