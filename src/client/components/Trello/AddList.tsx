import * as React from 'react';

interface AddListProps {
    onNameChange: React.ReactEventHandler;
    onSubmit: React.MouseEventHandler<HTMLButtonElement>;
    value?: string;
}

function AddList({onNameChange, onSubmit, value = ''}: AddListProps) {
    return (
        <form>
            <input onChange={onNameChange} value={value} className="list-name-input" placeholder="Enter list title..."/>
            <button onClick={onSubmit} className="primary">Add List</button>
        </form>
    );
}

export default AddList;