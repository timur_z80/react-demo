declare namespace NodeJS {
    interface MockedFetch {
        calledUrl: string;
        calledParams: {[key: string]: any};
        (url: string, params: {[key: string]: any}): Promise<{}>;
    }
    export interface Global {
        fetch: MockedFetch;
    }
}