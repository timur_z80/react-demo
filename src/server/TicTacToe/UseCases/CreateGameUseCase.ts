import {GameStorageGateway} from "../Gateways/GameStorageGateway";
import {BoundaryUser} from "../Boundaries/User";
import {Game} from "../Entities/Game";

export class CreateGameUseCase {

    constructor(private gateway: GameStorageGateway) {}

    execute(
        params: {players: [BoundaryUser, BoundaryUser]}
    ): string {

        const [player1, player2] =  params.players;
        const game = new Game(player1, player2);
        return this.gateway.addNewGame(game);
    }

}
