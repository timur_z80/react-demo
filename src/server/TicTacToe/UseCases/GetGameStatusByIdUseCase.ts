import { GameStorageGateway } from "../Gateways/GameStorageGateway";

export default class GetGameStatusByIdUseCase {
    constructor(private gateway: GameStorageGateway) {}

    execute(gameId: string): string {
        const game = this.gateway.findGame(gameId);
        return game.getStatus();
    }
}