import {GameStorageGateway} from "../Gateways/GameStorageGateway";
import {BoundaryUser} from "../Boundaries/User";
import BoundaryCoordinates from "../Boundaries/Coordinates";

export default class MakeMoveUseCase {
    constructor(private gateway: GameStorageGateway) {}

    execute(params: {gameId: string, player: BoundaryUser, move: BoundaryCoordinates}) {
        const game = this.gateway.findGame(params.gameId);
        game.move(params.player, params.move);
        this.gateway.updateGame(game);
    }
}