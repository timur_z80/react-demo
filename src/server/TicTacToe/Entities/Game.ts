import {BoundaryUser, Player} from "../Boundaries/User";
import {
    CantMoveAfterGameIsFinishedException, CoordinateIsAlreadyTakenException,
    ItsNotYourTurnException, PlayerIsNotAuthorized,
    PlayersShouldHaveDifferentTypes,
} from "../errors";
import BoundaryCoordinates from "../Boundaries/Coordinates";

export type TakenCoodinates = {
    'X': number[][], '0': number[][]
};

export class Game {
    private id:string|undefined;
    private lastMovedPlayer: BoundaryUser = {name: 'Default', type: Player.O};
    private takenCoordinates: TakenCoodinates = {'X': [], '0': []};
    private presenter: Presenter = new Presenter();

    constructor(private player1: BoundaryUser, private player2: BoundaryUser) {
        if (player1.type === player2.type) {
            throw new PlayersShouldHaveDifferentTypes();
        }
    }

    move(player: BoundaryUser, coordinates: BoundaryCoordinates) {
        const {X, Y} = coordinates;

        if(player.name !== this.player1.name && player.name !== this.player2.name) {
            throw new PlayerIsNotAuthorized('You are not authorized to play');
        }

        this.assertThisPlayerTurn(player);
        this.assertPreviousUserHasntWon();
        this.assertCoordinatesAreAvailable(X, Y);
        this.memorizeMoveCoordinates(player, X, Y);

        this.lastMovedPlayer = player;
    }

    setId(id: string) {
        this.id = id;
    }

    getId() {
        return this.id;
    }

    setPlayerX(player: BoundaryUser) {
        this.player1 = this.getPlayerCopy(player);
    }

    setPlayerO(player: BoundaryUser) {
        this.player2 = this.getPlayerCopy(player);
    }

    getPlayerX(): BoundaryUser {
        return this.player1;
    }

    getPlayerO(): BoundaryUser {
        return this.player2;
    }

    getCoordinates() {
        return this.takenCoordinates;
    }

    setCoordinates(taken: TakenCoodinates) {
        this.takenCoordinates = taken;
    }

    getLastMoved() {
        return this.lastMovedPlayer;
    }

    setLastMoved(player: BoundaryUser) {
        this.lastMovedPlayer = player;
    }

    private getPlayerCopy(player: BoundaryUser):BoundaryUser {
        return Object.assign(player);
    }

    describeSelf() {
        return {
            gameId: this.id,
            player1: this.getPlayerCopy(this.player1),
            player2: this.getPlayerCopy(this.player2),
            coordinates: this.getCoordinates(),
            lastMoved: this.getLastMoved()
        }
    }

    public getStatus(): string {
        if(this.checkIfPlayerWon(this.player1)){
            return this.presenter.getWonMessage(this.player1);
        }else if(this.checkIfPlayerWon(this.player2)) {
            return this.presenter.getWonMessage(this.player2);
        }
        if(this.allCellsAreFilled())
            return this.presenter.getDuesMessage();

        return this.presenter.getNextMoveMessage(this.getNextPlayer());
    }

    private assertThisPlayerTurn(player: BoundaryUser) {
        if (this.lastMovedPlayer.type === player.type) {
            throw new ItsNotYourTurnException('Its not your turn');
        }
    }

    private getNextPlayer() {
        return this.lastMovedPlayer.type === this.player1.type ? this.player2 : this.player1;
    }

    private allCellsAreFilled() {
        return this.takenCoordinates[Player.X].length + this.takenCoordinates[Player.O].length === 9;
    }

    private memorizeMoveCoordinates(player: BoundaryUser, x: number, y: number) {
        this.takenCoordinates[player.type].push([x, y]);
    }

    private assertCoordinatesAreAvailable(x: number, y: number) {
        if(this.checkUsedCoordinatesBy(this.player1, x, y) || this.checkUsedCoordinatesBy(this.player2, x, y)) {
            throw new CoordinateIsAlreadyTakenException('coordinates have already been taken');
        }
    }

    private checkUsedCoordinatesBy(player: BoundaryUser, x: number, y: number): boolean {
        let result = false;

        this.takenCoordinates[player.type].forEach((coordinate) => {
            if (coordinate.join('-') === [x,y].join('-')) {
                result =  true;
            }
        });

        return result;
    }

    private assertPreviousUserHasntWon(): any {
        if(this.checkIfPlayerWon(this.lastMovedPlayer)) {
            throw new CantMoveAfterGameIsFinishedException('the game is already finished')
        }
    }

    private checkIfPlayerWon(player: BoundaryUser) {
        let win = false;

        win = this.checkIfVariantsContainWonGame(
            this.fillWinningVariantsForPlayer(player.type)
        );

        return win;
    }

    private fillWinningVariantsForPlayer(player: Player) {
        let gameVariants: any =
            { 'X1': 0, 'X2': 0, 'X3': 0, 'Y1': 0, 'Y2': 0, 'Y3': 0, diagonal1: 0, diagonal2: 0 };

        this.takenCoordinates[player].forEach(coordinates => {
            gameVariants[this.getXCoordinateIndex(coordinates)] += 1;
            gameVariants[this.getYCoordinateIndex(coordinates)] += 1;
            gameVariants['diagonal1'] += this.leftBottomDiagonalGame(coordinates);
            gameVariants['diagonal2'] += this.leftTopDiagonalGame(coordinates);
        });

        return gameVariants;
    }

    private checkIfVariantsContainWonGame(gameVariations: {}) {
        let result = false;
        for (let key in gameVariations) {
            if (this.hasWonByOneOfAKindCoordinate(gameVariations, key))
                result = true;
        }
        return result;
    }

    private getYCoordinateIndex(coordinates: number[]):string {
        return 'Y' + coordinates[1];
    }

    private getXCoordinateIndex(coordinates: number[]):string {
        return 'X' + coordinates[0];
    }

    private hasWonByOneOfAKindCoordinate(check: any, key: string) {
        return check[key] === 3;
    }

    private leftBottomDiagonalGame(coordinates: number[]) {
        return coordinates[0] + coordinates[1] === 4 ?
            1 : 0;
    }

    private leftTopDiagonalGame(coordinates: number[]) {
        return (this.isXOneYOne(coordinates)
            || this.isXTwoYTwo(coordinates))
        || this.isXThreeYThree(coordinates) ?
            1 : 0;
    }

    private isXTwoYTwo(coordinates: number[]): boolean {
        return coordinates[0] === 2 && coordinates[1] === 2;
    }

    private isXThreeYThree(coordinates: number[]) {
        return coordinates[0] + coordinates[1]  === 6;
    }

    private isXOneYOne(coordinates: number[]) {
        return coordinates[0] + coordinates[1] === 2;
    }
}

class Presenter {
    public getNextMoveMessage(player: BoundaryUser):string {
        return `Waiting for ${player.name}'s to move`;
    }

    public getDuesMessage():string {
        return 'Draw game';
    }

    public getWonMessage(player: BoundaryUser) {
        return `${player.name} has won!`;
    }
}