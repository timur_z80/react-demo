export class ValidationError extends Error{}
export class InvalidPlayerTurnError extends Error{}
export class NotFound extends Error {}
export class ItsNotYourTurnException extends Error {}
export class CoordinateIsAlreadyTakenException extends Error {}
export class CantMoveAfterGameIsFinishedException extends Error {}
export class PlayersShouldHaveDifferentTypes extends Error{}
export class PlayerIsNotAuthorized extends Error{}