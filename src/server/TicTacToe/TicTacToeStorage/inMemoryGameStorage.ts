import {GameStorageGateway} from "../Gateways/GameStorageGateway";
import {TakenCoodinates} from "../Entities/Game";
import {NotFound} from "../errors";
import {BoundaryUser} from "../Boundaries/User";
import {Game} from "../Entities/Game";
import {Guid} from "guid-typescript";

type StoreData = {
    gameId: string,
    player1: BoundaryUser,
    player2: BoundaryUser,
    coordinates: TakenCoodinates,
    lastMoved: BoundaryUser
};

export class InMemoryGameStorage implements GameStorageGateway {
    static storage:Map<string, StoreData> = new Map();

    addNewGame(game: Game): string {
        const key = Guid.create().toString();

        game.setId(key);
        const gameData = game.describeSelf();
        const data = {
            gameId: gameData.gameId,
            player1: gameData.player1,
            player2: gameData.player2,
            coordinates: gameData.coordinates,
            lastMoved: gameData.lastMoved
        };

        InMemoryGameStorage.storage.set(key, data as StoreData);

        return key;
    }

    findGame(gameId: string): Game {
        if(!InMemoryGameStorage.storage.has(gameId)) {
            throw new NotFound();
        }

        const data = InMemoryGameStorage.storage.get(gameId) as StoreData;

        const game = new Game(data.player1, data.player2);

        game.setId(data.gameId as string);
        game.setCoordinates(data.coordinates);
        game.setLastMoved(data.lastMoved);

        return game;
    }

    updateGame(game: Game): void {
        if(!InMemoryGameStorage.storage.has(game.getId() as string)) {
            throw new NotFound();
        }

        const gameData = game.describeSelf();
        const data = {
            gameId: gameData.gameId,
            player1: gameData.player1,
            player2: gameData.player2,
            coordinates: gameData.coordinates,
            lastMoved: gameData.lastMoved
        };

        InMemoryGameStorage.storage.set(game.getId() as string, data as StoreData);
    }
}