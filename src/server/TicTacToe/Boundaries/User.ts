export interface BoundaryUser {
    name: string,
    type: Player
}

export enum Player {
    X = 'X',
    O = '0',
}
