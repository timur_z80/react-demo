import {Game} from "../Entities/Game";

export interface GameStorageGateway {
    addNewGame(game: Game): string;

    findGame(gameId: string): Game;

    updateGame(game: Game): void;
}