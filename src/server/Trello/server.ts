import express from 'express';
import cors from 'cors';
import { addList, addCard, updateCard, moveCard, getLists } from './TrelloController';

const defaultPort = '8080';
const app: express.Application = express();
const port: number = Number.parseInt(process.env.PORT || defaultPort);

app.use(cors({origin: true}), express.json());

app.post('/list/add', addList);
app.post('/card/add', addCard);
app.put('/card/update/:data', updateCard);
app.put('/card/move/:data', moveCard);
app.get('/lists/get', getLists);

app.listen(port, () => {
    console.log(`listening at http://localhost:${port}`);
});