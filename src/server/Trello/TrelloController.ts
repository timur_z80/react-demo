import { Lists } from 'src/client/components/Trello/Board/Board';

import {Request, Response} from 'express';
import uuid = require('uuid');

const lists: Lists = {};
interface RequestResponse {
    (request: Request, response: Response): void;
}

export const addList: RequestResponse = (req, resp) => {
    const {name} = req.body;
    const listId = uuid();
    lists[listId] = {name, cards: {}};

    return resp.json({listId});
}

export const addCard: RequestResponse = (req, resp) => {
    const {listId, name} = req.body;
    const cardId = uuid();

    lists[listId].cards[cardId] = name;
    return resp.json({cardId});
}

export const updateCard: RequestResponse = (req, resp) => {
    const {listId, cardId, text} = JSON.parse(req.params.data);
    lists[listId].cards[cardId] = text;
    return resp.json({success: true});
}

export const moveCard: RequestResponse = (req, resp) => {
    const {fromListId, toListId, cardId} = JSON.parse(req.params.data);

    const card = lists[fromListId].cards[cardId];
    delete lists[fromListId].cards[cardId];
    lists[toListId].cards[cardId] = card;

    return resp.json({success: true});
}

export const getLists: RequestResponse = (req, resp) => {
    return resp.json({lists});
}