import {Request, Response} from 'express';
import { CreateGameUseCase } from './TicTacToe/UseCases/CreateGameUseCase';
import { InMemoryGameStorage } from './TicTacToe/TicTacToeStorage/inMemoryGameStorage';
import MakeMoveUseCase from './TicTacToe/UseCases/MakeMoveUseCase';
import GetGameStatusByIdUseCase from './TicTacToe/UseCases/GetGameStatusByIdUseCase';

type RequestResponse = (request: Request, response: Response) => void;

const storage = new InMemoryGameStorage();

export const createGame: RequestResponse = (request, response) => {
    const players = JSON.parse(request.params.players);
    let result = '';
    try{
        result = (new CreateGameUseCase(storage)).execute({players});
        response.json({id: result});
    }catch(error) {
        response.json(error);
    }
}

export const move: RequestResponse = (request, response)  => {
    const moveParams = JSON.parse(request.params.move);
    const gameId = moveParams.gameId;
    try{
        (new MakeMoveUseCase(storage)).execute(moveParams);
        const status = (new GetGameStatusByIdUseCase(storage)).execute(gameId);
        const hasWon = isStatusOfWinnedGame(status);
        const isDraw = isStatusOfDrawGame(status);
        const winnerName = hasWon ? getWinnerNameFromStatus(status) : '';
        response.json({status, hasWon, isDraw, winnerName});
    }catch(error){
        response.json(error);
    }
}

const isStatusOfWinnedGame = (status: string) => {
    return status.toLowerCase().includes('won');
}

const isStatusOfDrawGame = (status: string) => {
    return status.toLowerCase().includes('draw');
}

const getWinnerNameFromStatus = (status: string) => {
    const firstSpaceIndex = status.indexOf(' ');
    if(firstSpaceIndex < 0) return '';
    return status.slice(0, firstSpaceIndex);
}