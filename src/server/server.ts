import express from 'express';
import { addPerson, getPeople } from './PersonController';
import { createGame, move } from './TicTacToeController';
import cors from 'cors';

const defaultPort = '8080';
const app: express.Application = express();
const port: number = Number.parseInt(process.env.PORT || defaultPort);

app.use(cors({origin: true}));

app.use('/add/:person', addPerson);
app.use('/people', getPeople);

// tic -tac - toe
app.use('/ttt/create-game/:players', createGame);
app.use('/ttt/move/:move', move);

app.listen(port, () => {
    console.log(`listening at http://localhost:${port}`);
})