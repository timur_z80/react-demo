declare namespace NodeJS {
    interface MockedFetch {
        calledUrl: string;
        (url: string): Promise<{}>;
    }
    export interface Global {
        fetch: MockedFetch;
    }
}